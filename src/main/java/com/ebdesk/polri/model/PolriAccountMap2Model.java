package com.ebdesk.polri.model;

import java.io.Serializable;
import java.util.List;

public class PolriAccountMap2Model implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String idAccount;
	private String idIssue;
	private String labelIssue;
	private List<String> idPost;

	public String getIdAccount() {
		return idAccount;
	}

	public void setIdAccount(String idAccount) {
		this.idAccount = idAccount;
	}

	public String getIdIssue() {
		return idIssue;
	}

	public void setIdIssue(String idIssue) {
		this.idIssue = idIssue;
	}

	public String getLabelIssue() {
		return labelIssue;
	}

	public void setLabelIssue(String labelIssue) {
		this.labelIssue = labelIssue;
	}

	public List<String> getIdPost() {
		return idPost;
	}

	public void setIdPost(List<String> idPost) {
		this.idPost = idPost;
	}

}
