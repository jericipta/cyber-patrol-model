package com.ebdesk.polri.model.instagram;

public class InstaDisplayResourcesModel {
	private String config_height;
	private String src;
	private String config_width;

	public String getConfig_height() {
		return config_height;
	}

	public void setConfig_height(String config_height) {
		this.config_height = config_height;
	}

	public String getSrc() {
		return src;
	}

	public void setSrc(String src) {
		this.src = src;
	}

	public String getConfig_width() {
		return config_width;
	}

	public void setConfig_width(String config_width) {
		this.config_width = config_width;
	}

}
