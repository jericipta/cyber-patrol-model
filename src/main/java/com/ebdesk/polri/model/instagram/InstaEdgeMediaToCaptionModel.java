package com.ebdesk.polri.model.instagram;

import java.util.List;

public class InstaEdgeMediaToCaptionModel {
	private List<InstaEdgesModel> edges;

	public List<InstaEdgesModel> getEdges() {
		return edges;
	}

	public void setEdges(List<InstaEdgesModel> edges) {
		this.edges = edges;
	}
}
