package com.ebdesk.polri.model.instagram;

public class InstaEdgesModel {
	private InstaNodeModel node;

	public InstaNodeModel getNode() {
		return node;
	}

	public void setNode(InstaNodeModel node) {
		this.node = node;
	}

}
