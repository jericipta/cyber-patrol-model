package com.ebdesk.polri.model.instagram;

import java.util.List;

public class InstagramModel {
	private String username;
	private String crawling_date;
	private String post_code;
	private String post_comment;
	private String post_like;
	private String post_picture;
	private InstaOwnerModel owner;
	private String user_id;
	private List<String> hashtags;
	private InstaRawDataModel raw_data;
	private String profile_pic;
	private String post_desc;
	private String post_id;
	private String post_date;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getCrawling_date() {
		return crawling_date;
	}

	public void setCrawling_date(String crawling_date) {
		this.crawling_date = crawling_date;
	}

	public String getPost_code() {
		return post_code;
	}

	public void setPost_code(String post_code) {
		this.post_code = post_code;
	}

	public String getPost_comment() {
		return post_comment;
	}

	public void setPost_comment(String post_comment) {
		this.post_comment = post_comment;
	}

	public String getPost_like() {
		return post_like;
	}

	public void setPost_like(String post_like) {
		this.post_like = post_like;
	}

	public String getPost_picture() {
		return post_picture;
	}

	public void setPost_picture(String post_picture) {
		this.post_picture = post_picture;
	}

	public InstaOwnerModel getOwner() {
		return owner;
	}

	public void setOwner(InstaOwnerModel owner) {
		this.owner = owner;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public List<String> getHashtags() {
		return hashtags;
	}

	public void setHashtags(List<String> hashtags) {
		this.hashtags = hashtags;
	}

	public InstaRawDataModel getRaw_data() {
		return raw_data;
	}

	public void setRaw_data(InstaRawDataModel raw_data) {
		this.raw_data = raw_data;
	}

	public String getProfile_pic() {
		return profile_pic;
	}

	public void setProfile_pic(String profile_pic) {
		this.profile_pic = profile_pic;
	}

	public String getPost_desc() {
		return post_desc;
	}

	public void setPost_desc(String post_desc) {
		this.post_desc = post_desc;
	}

	public String getPost_id() {
		return post_id;
	}

	public void setPost_id(String post_id) {
		this.post_id = post_id;
	}

	public String getPost_date() {
		return post_date;
	}

	public void setPost_date(String post_date) {
		this.post_date = post_date;
	}

}
