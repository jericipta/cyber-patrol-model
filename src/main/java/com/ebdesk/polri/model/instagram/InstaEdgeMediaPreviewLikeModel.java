package com.ebdesk.polri.model.instagram;

import java.util.List;

public class InstaEdgeMediaPreviewLikeModel {
    private List<InstaEdgesModel> edges;
	private String count;

	public List<InstaEdgesModel> getEdges() {
		return edges;
	}

	public void setEdges(List<InstaEdgesModel> edges) {
		this.edges = edges;
	}

	public String getCount() {
		return count;
	}

	public void setCount(String count) {
		this.count = count;
	}
}
