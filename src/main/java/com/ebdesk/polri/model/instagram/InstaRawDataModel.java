package com.ebdesk.polri.model.instagram;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class InstaRawDataModel {
	private InstaEdgeMediaToCaptionModel edge_media_to_caption;
	private String comments_disabled;
	private String __typename;
	private InstaLocationModel location;
	private InstaEdgeMediaToCommentModel edge_media_to_comment;
	private String keyword_id;
	private String viewer_has_saved_to_collection;
	private String viewer_has_saved;
	private List<InstaDisplayResourcesModel> display_resources;
	private InstaEdgeWebMediaToRelatedMediaModel edge_web_media_to_related_media;
	private String display_url;
	private String tracking_token;
	private String gating_info;
	private InstaEdgeMediaToTaggedUserModel edge_media_to_tagged_user;
	private String media_preview;
	private String id;
	private String is_video;
	private String viewer_has_liked;
	private InstaEdgeMediaToSponsorUserModel edge_media_to_sponsor_user;
	private String is_ad;
	private InstaOwnerModel owner;
	private InstaDimensionsModel dimensions;
	private String taken_at_timestamp;
	private String shortcode;
	private String should_log_client_event;
	private String caption_is_edited;
	private InstaEdgeMediaPreviewLikeModel edge_media_preview_like;
	private InstaEdgeSideCarToChildrenModel edge_sidecar_to_children;
	private String video_url;
	private Integer video_view_count;
	
	public Integer getVideo_view_count() {
		return video_view_count;
	}

	public void setVideo_view_count(Integer video_view_count) {
		this.video_view_count = video_view_count;
	}
	
	public String getVideo_url() {
		return video_url;
	}

	public void setVideo_url(String video_url) {
		this.video_url = video_url;
	}

	public InstaEdgeSideCarToChildrenModel getEdge_sidecar_to_children() {
		return edge_sidecar_to_children;
	}
	
	public void setEdge_sidecar_to_children(InstaEdgeSideCarToChildrenModel edge_sidecar_to_children) {
		this.edge_sidecar_to_children = edge_sidecar_to_children;
	}

	public InstaEdgeMediaToCaptionModel getEdge_media_to_caption() {
		return edge_media_to_caption;
	}

	public void setEdge_media_to_caption(InstaEdgeMediaToCaptionModel edge_media_to_caption) {
		this.edge_media_to_caption = edge_media_to_caption;
	}

	public String getComments_disabled() {
		return comments_disabled;
	}

	public void setComments_disabled(String comments_disabled) {
		this.comments_disabled = comments_disabled;
	}

	public String get__typename() {
		return __typename;
	}

	public void set__typename(String __typename) {
		this.__typename = __typename;
	}

	public InstaLocationModel getLocation() {
		return location;
	}

	public void setLocation(InstaLocationModel location) {
		this.location = location;
	}

	public InstaEdgeMediaToCommentModel getEdge_media_to_comment() {
		return edge_media_to_comment;
	}

	public void setEdge_media_to_comment(InstaEdgeMediaToCommentModel edge_media_to_comment) {
		this.edge_media_to_comment = edge_media_to_comment;
	}

	public String getKeyword_id() {
		return keyword_id;
	}

	public void setKeyword_id(String keyword_id) {
		this.keyword_id = keyword_id;
	}

	public String getViewer_has_saved_to_collection() {
		return viewer_has_saved_to_collection;
	}

	public void setViewer_has_saved_to_collection(String viewer_has_saved_to_collection) {
		this.viewer_has_saved_to_collection = viewer_has_saved_to_collection;
	}

	public String getViewer_has_saved() {
		return viewer_has_saved;
	}

	public void setViewer_has_saved(String viewer_has_saved) {
		this.viewer_has_saved = viewer_has_saved;
	}

	public List<InstaDisplayResourcesModel> getDisplay_resources() {
		return display_resources;
	}

	public void setDisplay_resources(List<InstaDisplayResourcesModel> display_resources) {
		this.display_resources = display_resources;
	}

	public InstaEdgeWebMediaToRelatedMediaModel getEdge_web_media_to_related_media() {
		return edge_web_media_to_related_media;
	}

	public void setEdge_web_media_to_related_media(
			InstaEdgeWebMediaToRelatedMediaModel edge_web_media_to_related_media) {
		this.edge_web_media_to_related_media = edge_web_media_to_related_media;
	}

	public String getDisplay_url() {
		return display_url;
	}

	public void setDisplay_url(String display_url) {
		this.display_url = display_url;
	}

	public String getTracking_token() {
		return tracking_token;
	}

	public void setTracking_token(String tracking_token) {
		this.tracking_token = tracking_token;
	}

	public String getGating_info() {
		return gating_info;
	}

	public void setGating_info(String gating_info) {
		this.gating_info = gating_info;
	}

	public InstaEdgeMediaToTaggedUserModel getEdge_media_to_tagged_user() {
		return edge_media_to_tagged_user;
	}

	public void setEdge_media_to_tagged_user(InstaEdgeMediaToTaggedUserModel edge_media_to_tagged_user) {
		this.edge_media_to_tagged_user = edge_media_to_tagged_user;
	}

	public String getMedia_preview() {
		return media_preview;
	}

	public void setMedia_preview(String media_preview) {
		this.media_preview = media_preview;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIs_video() {
		return is_video;
	}

	public void setIs_video(String is_video) {
		this.is_video = is_video;
	}

	public String getViewer_has_liked() {
		return viewer_has_liked;
	}

	public void setViewer_has_liked(String viewer_has_liked) {
		this.viewer_has_liked = viewer_has_liked;
	}

	public InstaEdgeMediaToSponsorUserModel getEdge_media_to_sponsor_user() {
		return edge_media_to_sponsor_user;
	}

	public void setEdge_media_to_sponsor_user(InstaEdgeMediaToSponsorUserModel edge_media_to_sponsor_user) {
		this.edge_media_to_sponsor_user = edge_media_to_sponsor_user;
	}

	public String getIs_ad() {
		return is_ad;
	}

	public void setIs_ad(String is_ad) {
		this.is_ad = is_ad;
	}

	public InstaOwnerModel getOwner() {
		return owner;
	}

	public void setOwner(InstaOwnerModel owner) {
		this.owner = owner;
	}

	public InstaDimensionsModel getDimensions() {
		return dimensions;
	}

	public void setDimensions(InstaDimensionsModel dimensions) {
		this.dimensions = dimensions;
	}

	public String getTaken_at_timestamp() {
		return taken_at_timestamp;
	}

	public void setTaken_at_timestamp(String taken_at_timestamp) {
		this.taken_at_timestamp = taken_at_timestamp;
	}

	public String getShortcode() {
		return shortcode;
	}

	public void setShortcode(String shortcode) {
		this.shortcode = shortcode;
	}

	public String getShould_log_client_event() {
		return should_log_client_event;
	}

	public void setShould_log_client_event(String should_log_client_event) {
		this.should_log_client_event = should_log_client_event;
	}

	public String getCaption_is_edited() {
		return caption_is_edited;
	}

	public void setCaption_is_edited(String caption_is_edited) {
		this.caption_is_edited = caption_is_edited;
	}

	public InstaEdgeMediaPreviewLikeModel getEdge_media_preview_like() {
		return edge_media_preview_like;
	}

	public void setEdge_media_preview_like(InstaEdgeMediaPreviewLikeModel edge_media_preview_like) {
		this.edge_media_preview_like = edge_media_preview_like;
	}
}
