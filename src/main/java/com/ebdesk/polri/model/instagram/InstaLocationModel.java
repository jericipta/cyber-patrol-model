package com.ebdesk.polri.model.instagram;

public class InstaLocationModel {
	private Boolean has_public_page;
	private String slug;
	private String name;
	private String id;

	public Boolean getHas_public_page() {
		return has_public_page;
	}

	public void setHas_public_page(Boolean has_public_page) {
		this.has_public_page = has_public_page;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
