package com.ebdesk.polri.model.instagram;

public class InstaOwnerModel {
	private String id;
	private String profile_pic_url;
	private String username;
	private String blocked_by_viewer;
	private String is_verified;
	private String is_private;
	private String has_blocked_viewer;
	private String is_unpublished;
	private String followed_by_viewer;
	private String requested_by_viewer;
	private String full_name;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getProfile_pic_url() {
		return profile_pic_url;
	}

	public void setProfile_pic_url(String profile_pic_url) {
		this.profile_pic_url = profile_pic_url;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getBlocked_by_viewer() {
		return blocked_by_viewer;
	}

	public void setBlocked_by_viewer(String blocked_by_viewer) {
		this.blocked_by_viewer = blocked_by_viewer;
	}

	public String getIs_verified() {
		return is_verified;
	}

	public void setIs_verified(String is_verified) {
		this.is_verified = is_verified;
	}

	public String getIs_private() {
		return is_private;
	}

	public void setIs_private(String is_private) {
		this.is_private = is_private;
	}

	public String getHas_blocked_viewer() {
		return has_blocked_viewer;
	}

	public void setHas_blocked_viewer(String has_blocked_viewer) {
		this.has_blocked_viewer = has_blocked_viewer;
	}

	public String getIs_unpublished() {
		return is_unpublished;
	}

	public void setIs_unpublished(String is_unpublished) {
		this.is_unpublished = is_unpublished;
	}

	public String getFollowed_by_viewer() {
		return followed_by_viewer;
	}

	public void setFollowed_by_viewer(String followed_by_viewer) {
		this.followed_by_viewer = followed_by_viewer;
	}

	public String getRequested_by_viewer() {
		return requested_by_viewer;
	}

	public void setRequested_by_viewer(String requested_by_viewer) {
		this.requested_by_viewer = requested_by_viewer;
	}

	public String getFull_name() {
		return full_name;
	}

	public void setFull_name(String full_name) {
		this.full_name = full_name;
	}

}
