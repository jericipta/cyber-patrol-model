package com.ebdesk.polri.model;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PolriModel implements Serializable {

	private static final long serialVersionUID = 1L;

	private String topic;
	private String source;
	private String status;
	private PolriPostModel tweetData;
	private PolriPostModel retweetData;
	private DemographyEnhancedModel demography;
	private List<String> issue;
	private List<EnhAnnotationModel> annotation;
	private List<String> category;
	private String inReplyToStatusId;
	private String inReplyToUserId;
	private String inReplyToScreenName;

	// private String post;
	// private String idPost;
	// private String created_at;
	// private String lang;
	// private List<String> mentions;
	// private List<String> urls;
	// private String idAccount;
	// private String name;
	// private String name2;
	// private String image;
	// private int friends;
	// private int followers;
	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public PolriPostModel getTweetData() {
		return tweetData;
	}

	public void setTweetData(PolriPostModel tweetData) {
		this.tweetData = tweetData;
	}

	public PolriPostModel getRetweetData() {
		return retweetData;
	}

	public void setRetweetData(PolriPostModel retweetData) {
		this.retweetData = retweetData;
	}

	public DemographyEnhancedModel getDemography() {
		return demography;
	}

	public void setDemography(DemographyEnhancedModel demography) {
		this.demography = demography;
	}

	public List<String> getIssue() {
		return issue;
	}

	public void setIssue(List<String> issue) {
		this.issue = issue;
	}

	public List<EnhAnnotationModel> getAnnotation() {
		return annotation;
	}

	public void setAnnotation(List<EnhAnnotationModel> annotation) {
		this.annotation = annotation;
	}

	public List<String> getCategory() {
		return category;
	}

	public void setCategory(List<String> category) {
		this.category = category;
	}

	public String getInReplyToStatusId() {
		return inReplyToStatusId;
	}

	public void setInReplyToStatusId(String inReplyToStatusId) {
		this.inReplyToStatusId = inReplyToStatusId;
	}

	public String getInReplyToUserId() {
		return inReplyToUserId;
	}

	public void setInReplyToUserId(String inReplyToUserId) {
		this.inReplyToUserId = inReplyToUserId;
	}

	public String getInReplyToScreenName() {
		return inReplyToScreenName;
	}

	public void setInReplyToScreenName(String inReplyToScreenName) {
		this.inReplyToScreenName = inReplyToScreenName;
	}

}
