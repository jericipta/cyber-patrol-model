package com.ebdesk.polri.model;

public class PolriCategoryModel {
	private String id;
	private String updated_at;
	private String keyword;
	private String created_at;
	private String ket;
	private String type;
	private String cat_name;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getKet() {
		return ket;
	}

	public void setKet(String ket) {
		this.ket = ket;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCat_name() {
		return cat_name;
	}

	public void setCat_name(String cat_name) {
		this.cat_name = cat_name;
	}

	@Override
	public String toString() {
		return "ClassPojo [id = " + id + ", updated_at = " + updated_at + ", keyword = " + keyword + ", created_at = "
				+ created_at + ", ket = " + ket + ", type = " + type + ", cat_name = " + cat_name + "]";
	}
}
