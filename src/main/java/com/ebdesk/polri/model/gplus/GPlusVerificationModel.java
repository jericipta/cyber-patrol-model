package com.ebdesk.polri.model.gplus;

public class GPlusVerificationModel {
	private String adHocVerified;

	public String getAdHocVerified() {
		return adHocVerified;
	}

	public void setAdHocVerified(String adHocVerified) {
		this.adHocVerified = adHocVerified;
	}

}
