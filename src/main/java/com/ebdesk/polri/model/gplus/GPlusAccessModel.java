package com.ebdesk.polri.model.gplus;

import java.util.List;

public class GPlusAccessModel {
	private List<GPlusItemsModel> items;
	private String description;
	private String kind;

	public List<GPlusItemsModel> getItems() {
		return items;
	}

	public void setItems(List<GPlusItemsModel> items) {
		this.items = items;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getKind() {
		return kind;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}
}
