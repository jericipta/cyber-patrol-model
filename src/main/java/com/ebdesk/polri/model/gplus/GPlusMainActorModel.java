package com.ebdesk.polri.model.gplus;

public class GPlusMainActorModel {
	private String id;
	private GPlusVerificationModel verification;
	private GPlusImageModel image;
	private String displayName;
	private String url;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public GPlusVerificationModel getVerification() {
		return verification;
	}

	public void setVerification(GPlusVerificationModel verification) {
		this.verification = verification;
	}

	public GPlusImageModel getImage() {
		return image;
	}

	public void setImage(GPlusImageModel image) {
		this.image = image;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
