package com.ebdesk.polri.model.gplus;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class GPlusAttachmentsModel {
	private String displayName;
	private String objectType;
	private String url;
	private GPlusFullImageModel fullImage;
	private GPlusImageModel image;
	private String content;

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public GPlusImageModel getImage() {
		return image;
	}

	public void setImage(GPlusImageModel image) {
		this.image = image;
	}

	public GPlusFullImageModel getFullImage() {
		return fullImage;
	}

	public void setFullImage(GPlusFullImageModel fullImage) {
		this.fullImage = fullImage;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getObjectType() {
		return objectType;
	}

	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
