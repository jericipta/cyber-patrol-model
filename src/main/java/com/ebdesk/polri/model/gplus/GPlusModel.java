package com.ebdesk.polri.model.gplus;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class GPlusModel {
	private String etag;
	private String keyword_id;
	private GPlusProviderModel provider;
	private GPlusObjectModel object;
	private GPlusMainActorModel actor;
	private GPlusAccessModel access;
	private String kind;
	private String url;
	private String id;
	private String title;
	private String verb;
	private String updated;
	private String published;

	public String getEtag() {
		return etag;
	}

	public void setEtag(String etag) {
		this.etag = etag;
	}

	public String getKeyword_id() {
		return keyword_id;
	}

	public void setKeyword_id(String keyword_id) {
		this.keyword_id = keyword_id;
	}

	public GPlusProviderModel getProvider() {
		return provider;
	}

	public void setProvider(GPlusProviderModel provider) {
		this.provider = provider;
	}

	public GPlusObjectModel getObject() {
		return object;
	}

	public void setObject(GPlusObjectModel object) {
		this.object = object;
	}

	public GPlusMainActorModel getActor() {
		return actor;
	}

	public void setActor(GPlusMainActorModel actor) {
		this.actor = actor;
	}

	public GPlusAccessModel getAccess() {
		return access;
	}

	public void setAccess(GPlusAccessModel access) {
		this.access = access;
	}

	public String getKind() {
		return kind;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getVerb() {
		return verb;
	}

	public void setVerb(String verb) {
		this.verb = verb;
	}

	public String getUpdated() {
		return updated;
	}

	public void setUpdated(String updated) {
		this.updated = updated;
	}

	public String getPublished() {
		return published;
	}

	public void setPublished(String published) {
		this.published = published;
	}

}
