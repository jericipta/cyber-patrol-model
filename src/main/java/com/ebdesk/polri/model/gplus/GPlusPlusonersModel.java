package com.ebdesk.polri.model.gplus;

public class GPlusPlusonersModel {
	private String totalItems;
	private String selfLink;

	public String getTotalItems() {
		return totalItems;
	}

	public void setTotalItems(String totalItems) {
		this.totalItems = totalItems;
	}

	public String getSelfLink() {
		return selfLink;
	}

	public void setSelfLink(String selfLink) {
		this.selfLink = selfLink;
	}

}
