package com.ebdesk.polri.model.gplus;

import java.util.List;

public class GPlusObjectModel {
	private String content;
	private GPlusRepliesModel replies;
	private GPlusPlusonersModel plusoners;
	private GPlusActorModel actor;
	private List<GPlusAttachmentsModel> attachments;
	private String objectType;
	private GPlusResharersModel resharers;
	private String url;
	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public GPlusRepliesModel getReplies() {
		return replies;
	}

	public void setReplies(GPlusRepliesModel replies) {
		this.replies = replies;
	}

	public GPlusPlusonersModel getPlusoners() {
		return plusoners;
	}

	public void setPlusoners(GPlusPlusonersModel plusoners) {
		this.plusoners = plusoners;
	}

	public GPlusActorModel getActor() {
		return actor;
	}

	public void setActor(GPlusActorModel actor) {
		this.actor = actor;
	}

	public List<GPlusAttachmentsModel> getAttachments() {
		return attachments;
	}

	public void setAttachments(List<GPlusAttachmentsModel> attachments) {
		this.attachments = attachments;
	}

	public String getObjectType() {
		return objectType;
	}

	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}

	public GPlusResharersModel getResharers() {
		return resharers;
	}

	public void setResharers(GPlusResharersModel resharers) {
		this.resharers = resharers;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
