package com.ebdesk.polri.model.facebook;

public class FacebookMediaModel {
	private FacebookImageModel image;
	
	public FacebookImageModel getImage() {
		return image;
	}

	public void setImage(FacebookImageModel image) {
		this.image = image;
	}

	@Override
	public String toString() {
		return "ClassPojo [image = " + image + "]";
	}
}
