package com.ebdesk.polri.model.facebook;

public class FacebookDescriptionTagsModel {
	private String id;
	private String name;
	private String length;
	private String type;
	private String offset;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLength() {
		return length;
	}

	public void setLength(String length) {
		this.length = length;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getOffset() {
		return offset;
	}

	public void setOffset(String offset) {
		this.offset = offset;
	}

	@Override
	public String toString() {
		return "ClassPojo [id = " + id + ", name = " + name + ", length = " + length + ", type = " + type
				+ ", offset = " + offset + "]";
	}
}
