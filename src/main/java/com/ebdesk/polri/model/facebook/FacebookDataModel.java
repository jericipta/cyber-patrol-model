package com.ebdesk.polri.model.facebook;

import java.util.List;

public class FacebookDataModel {
	private String title;
	private String description;
	private FacebookTargetModel target;
	private String type;
	private FacebookMediaModel media;
	private String url;
	private Object subattachments;
	private List<FacebookDescriptionTagsModel> description_tags;

	public List<FacebookDescriptionTagsModel> getDescription_tags() {
		return description_tags;
	}

	public void setDescription_tags(List<FacebookDescriptionTagsModel> description_tags) {
		this.description_tags = description_tags;
	}

	public Object getSubattachments() {
		return subattachments;
	}

	public void setSubattachments(Object subattachments) {
		this.subattachments = subattachments;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public FacebookTargetModel getTarget() {
		return target;
	}

	public void setTarget(FacebookTargetModel target) {
		this.target = target;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public FacebookMediaModel getMedia() {
		return media;
	}

	public void setMedia(FacebookMediaModel media) {
		this.media = media;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
