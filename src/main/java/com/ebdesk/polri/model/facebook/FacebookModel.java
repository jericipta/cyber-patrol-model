package com.ebdesk.polri.model.facebook;

import java.util.List;

public class FacebookModel {
	private String reaction;
	private String link;
	private FacebookFromModel from;
	private String like;
	private String type;
	private String id;
	private List<String> picture;
	private String message;
	private String story;
	private String description;
	private String name;
	private List<FacebookFromModel> tagged;
	private String created_time;
	private FacebookMetadataModel metadata;
	private FacebookCommentsModel comments;
	private String caption;
	private String status_type;
	private FacebookLikesModel likes;
	private FacebookAttachmentsModel attachments;
	private FacebookSharesModel shares;
	private FacebookUserModel user;
	private String group_id;
	
	public String getGroup_id() {
		return group_id;
	}

	public void setGroup_id(String group_id) {
		this.group_id = group_id;
	}

	public String getReaction() {
		return reaction;
	}

	public void setReaction(String reaction) {
		this.reaction = reaction;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public FacebookFromModel getFrom() {
		return from;
	}

	public void setFrom(FacebookFromModel from) {
		this.from = from;
	}

	public String getLike() {
		return like;
	}

	public void setLike(String like) {
		this.like = like;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<String> getPicture() {
		return picture;
	}

	public void setPicture(List<String> picture) {
		this.picture = picture;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getStory() {
		return story;
	}

	public void setStory(String story) {
		this.story = story;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<FacebookFromModel> getTagged() {
		return tagged;
	}

	public void setTagged(List<FacebookFromModel> tagged) {
		this.tagged = tagged;
	}

	public String getCreated_time() {
		return created_time;
	}

	public void setCreated_time(String created_time) {
		this.created_time = created_time;
	}

	public FacebookMetadataModel getMetadata() {
		return metadata;
	}

	public void setMetadata(FacebookMetadataModel metadata) {
		this.metadata = metadata;
	}

	public FacebookCommentsModel getComments() {
		return comments;
	}

	public void setComments(FacebookCommentsModel comments) {
		this.comments = comments;
	}

	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public String getStatus_type() {
		return status_type;
	}

	public void setStatus_type(String status_type) {
		this.status_type = status_type;
	}

	public FacebookLikesModel getLikes() {
		return likes;
	}

	public void setLikes(FacebookLikesModel likes) {
		this.likes = likes;
	}

	public FacebookAttachmentsModel getAttachments() {
		return attachments;
	}

	public void setAttachments(FacebookAttachmentsModel attachments) {
		this.attachments = attachments;
	}

	public FacebookSharesModel getShares() {
		return shares;
	}

	public void setShares(FacebookSharesModel shares) {
		this.shares = shares;
	}

	public FacebookUserModel getUser() {
		return user;
	}

	public void setUser(FacebookUserModel user) {
		this.user = user;
	}

}
