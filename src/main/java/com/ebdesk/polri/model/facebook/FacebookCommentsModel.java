package com.ebdesk.polri.model.facebook;

public class FacebookCommentsModel {
	private String count;

	public String getCount() {
		return count;
	}

	public void setCount(String count) {
		this.count = count;
	}

	@Override
	public String toString() {
		return "ClassPojo [count = " + count + "]";
	}
}
