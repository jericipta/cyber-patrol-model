package com.ebdesk.polri.model.facebook;

public class FacebookUserModel {
	private FacebookPictureModel picture;
	private String id;
	private String fan_count;
	private String username;
	private String category;
	private FacebookLocationModel location;
	private String website;
	private String awards;
	private Object link;
	private String name;
	private String about;
	private String description;
	private String created_time;
	private FacebookMetadataModel metadata;
	private String reaction;
	private String like;

	public String getLike() {
		return like;
	}

	public void setLike(String like) {
		this.like = like;
	}

	public String getReaction() {
		return reaction;
	}

	public void setReaction(String reaction) {
		this.reaction = reaction;
	}

	public FacebookMetadataModel getMetadata() {
		return metadata;
	}

	public void setMetadata(FacebookMetadataModel metadata) {
		this.metadata = metadata;
	}

	public String getCreated_time() {
		return created_time;
	}

	public void setCreated_time(String created_time) {
		this.created_time = created_time;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	private String talking_about_count;

	public FacebookPictureModel getPicture() {
		return picture;
	}

	public void setPicture(FacebookPictureModel picture) {
		this.picture = picture;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFan_count() {
		return fan_count;
	}

	public void setFan_count(String fan_count) {
		this.fan_count = fan_count;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public FacebookLocationModel getLocation() {
		return location;
	}

	public void setLocation(FacebookLocationModel location) {
		this.location = location;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getAwards() {
		return awards;
	}

	public void setAwards(String awards) {
		this.awards = awards;
	}

	public Object getLink() {
		return link;
	}

	public void setLink(Object link) {
		this.link = link;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAbout() {
		return about;
	}

	public void setAbout(String about) {
		this.about = about;
	}

	public String getTalking_about_count() {
		return talking_about_count;
	}

	public void setTalking_about_count(String talking_about_count) {
		this.talking_about_count = talking_about_count;
	}

	@Override
	public String toString() {
		return "ClassPojo [picture = " + picture + ", id = " + id + ", fan_count = " + fan_count + ", username = "
				+ username + ", category = " + category + ", location = " + location + ", website = " + website
				+ ", awards = " + awards + ", link = " + link + ", name = " + name + ", about = " + about
				+ ", talking_about_count = " + talking_about_count + "]";
	}
}
