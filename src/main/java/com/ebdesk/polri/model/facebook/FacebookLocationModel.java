package com.ebdesk.polri.model.facebook;

public class FacebookLocationModel {
	private String zip;
	private String street;
	private String longitude;
	private String latitude;
	private String city;
	private String country;
	private String state;

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@Override
	public String toString() {
		return "ClassPojo [zip = " + zip + ", street = " + street + ", longitude = " + longitude + ", latitude = "
				+ latitude + ", city = " + city + ", country = " + country + "]";
	}
}
