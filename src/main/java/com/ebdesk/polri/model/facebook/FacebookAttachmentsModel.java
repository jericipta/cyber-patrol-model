package com.ebdesk.polri.model.facebook;

import java.util.List;

public class FacebookAttachmentsModel {
	private List<FacebookDataModel> data;

	public List<FacebookDataModel> getData() {
		return data;
	}

	public void setData(List<FacebookDataModel> data) {
		this.data = data;
	}

}
