package com.ebdesk.polri.model.linkedin;

public class LinkedinModel {
	private String share_content;
	private String share_actor_name;
	private String actor_id;
	private String actor_name;
	private String keyword_id;
	private String image;
	private String article_share_url;
	private String type;
	private String date;
	private String share_actor_url;
	private String url;
	private String id;
	private String content;
	private String actor_url;
	private String share_actor_headline;
	private String article_share_headline;

	public String getShare_content() {
		return share_content;
	}

	public void setShare_content(String share_content) {
		this.share_content = share_content;
	}

	public String getShare_actor_name() {
		return share_actor_name;
	}

	public void setShare_actor_name(String share_actor_name) {
		this.share_actor_name = share_actor_name;
	}

	public String getActor_id() {
		return actor_id;
	}

	public void setActor_id(String actor_id) {
		this.actor_id = actor_id;
	}

	public String getActor_name() {
		return actor_name;
	}

	public void setActor_name(String actor_name) {
		this.actor_name = actor_name;
	}

	public String getKeyword_id() {
		return keyword_id;
	}

	public void setKeyword_id(String keyword_id) {
		this.keyword_id = keyword_id;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getArticle_share_url() {
		return article_share_url;
	}

	public void setArticle_share_url(String article_share_url) {
		this.article_share_url = article_share_url;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getShare_actor_url() {
		return share_actor_url;
	}

	public void setShare_actor_url(String share_actor_url) {
		this.share_actor_url = share_actor_url;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getActor_url() {
		return actor_url;
	}

	public void setActor_url(String actor_url) {
		this.actor_url = actor_url;
	}

	public String getShare_actor_headline() {
		return share_actor_headline;
	}

	public void setShare_actor_headline(String share_actor_headline) {
		this.share_actor_headline = share_actor_headline;
	}

	public String getArticle_share_headline() {
		return article_share_headline;
	}

	public void setArticle_share_headline(String article_share_headline) {
		this.article_share_headline = article_share_headline;
	}
}
