package com.ebdesk.polri.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class PolriPostModel implements Serializable {
	private static final long serialVersionUID = 1L;

	private String id;
	private PolriUserModel user;
	private String createdAt;
	private Date createdAtDate;
	private String postMessage;
	private List<String> mentions;
	private List<String> urls;
	private String lang;

	public Date getCreatedAtDate() {
		return createdAtDate;
	}

	public void setCreatedAtDate(Date createdAtDate) {
		this.createdAtDate = createdAtDate;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public PolriUserModel getUser() {
		return user;
	}

	public void setUser(PolriUserModel user) {
		this.user = user;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getPostMessage() {
		return postMessage;
	}

	public void setPostMessage(String postMessage) {
		this.postMessage = postMessage;
	}

	public List<String> getMentions() {
		return mentions;
	}

	public void setMentions(List<String> mentions) {
		this.mentions = mentions;
	}

	public List<String> getUrls() {
		return urls;
	}

	public void setUrls(List<String> urls) {
		this.urls = urls;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

}
