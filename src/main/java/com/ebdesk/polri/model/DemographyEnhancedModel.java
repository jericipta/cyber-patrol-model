package com.ebdesk.polri.model;

import java.io.Serializable;
import java.util.List;

public class DemographyEnhancedModel implements Serializable {

	private static final long serialVersionUID = 1L;
	private String gender;
	private List<String> location;
	private String age;
	private String religion;

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public List<String> getLocation() {
		return location;
	}

	public void setLocation(List<String> location) {
		this.location = location;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getReligion() {
		return religion;
	}

	public void setReligion(String religion) {
		this.religion = religion;
	}

}
