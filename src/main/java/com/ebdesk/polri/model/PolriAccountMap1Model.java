package com.ebdesk.polri.model;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class PolriAccountMap1Model implements Serializable {
	private static final long serialVersionUID = 1L;

	private String idIssue;
	private String labelIssue;
	private Map<String, List<String>> accountPosts;

	public String getIdIssue() {
		return idIssue;
	}

	public void setIdIssue(String idIssue) {
		this.idIssue = idIssue;
	}

	public String getLabelIssue() {
		return labelIssue;
	}

	public void setLabelIssue(String labelIssue) {
		this.labelIssue = labelIssue;
	}

	public Map<String, List<String>> getAccountPosts() {
		return accountPosts;
	}

	public void setAccountPosts(Map<String, List<String>> accountPosts) {
		this.accountPosts = accountPosts;
	}

}
