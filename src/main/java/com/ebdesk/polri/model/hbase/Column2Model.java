package com.ebdesk.polri.model.hbase;

import java.util.List;

import com.ebdesk.polri.model.PolriModel;

public class Column2Model {
	private String idIssue;
	private List<PolriModel> issueDetil;

	public String getIdIssue() {
		return idIssue;
	}

	public void setIdIssue(String idIssue) {
		this.idIssue = idIssue;
	}

	public List<PolriModel> getIssueDetil() {
		return issueDetil;
	}

	public void setIssueDetil(List<PolriModel> issueDetil) {
		this.issueDetil = issueDetil;
	}

}
