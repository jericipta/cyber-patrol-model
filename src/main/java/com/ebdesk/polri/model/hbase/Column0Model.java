package com.ebdesk.polri.model.hbase;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class Column0Model implements Serializable {
	private static final long serialVersionUID = 1L;
	private String id;
	private String idRow;
	private String issueName;
	private Integer totalPost;
	private Integer totalInfluencer;
	private float score;
	private List<String> listIssue;
	private List<String> listCategory;
	private List<Column0Model> listIssue2;
	private String status;
	private String medsos;

	public String getMedsos() {
		return medsos;
	}

	public void setMedsos(String medsos) {
		this.medsos = medsos;
	}

	public String getIdRow() {
		return idRow;
	}

	public void setIdRow(String idRow) {
		this.idRow = idRow;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<Column0Model> getListIssue2() {
		return listIssue2;
	}

	public void setListIssue2(List<Column0Model> listIssue2) {
		this.listIssue2 = listIssue2;
	}

	public List<String> getListIssue() {
		return listIssue;
	}

	public void setListIssue(List<String> listIssue) {
		this.listIssue = listIssue;
	}

	public List<String> getListCategory() {
		return listCategory;
	}

	public void setListCategory(List<String> listCategory) {
		this.listCategory = listCategory;
	}

	public float getScore() {
		return score;
	}

	public void setScore(float score) {
		this.score = score;
	}

	public Integer getTotalPost() {
		return totalPost;
	}

	public void setTotalPost(Integer totalPost) {
		this.totalPost = totalPost;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIssueName() {
		return issueName;
	}

	public void setIssueName(String issueName) {
		this.issueName = issueName;
	}

	public Integer getTotalInfluencer() {
		return totalInfluencer;
	}

	public void setTotalInfluencer(Integer totalInfluencer) {
		this.totalInfluencer = totalInfluencer;
	}

}
