package com.ebdesk.polri.model.hbase;

public class HbaseModel {
	public String rowKey;
	public String qualifier;
	public String value;
	
//	public HbaseModel(String rowKey, String qualifier, String value) {
//		super();
//		this.rowKey = rowKey;
//		this.qualifier = qualifier;
//		this.value = value;
//	}
	public String getRowKey() {
		return rowKey;
	}
	public void setRowKey(String rowKey) {
		this.rowKey = rowKey;
	}
	public String getQualifier() {
		return qualifier;
	}
	public void setQualifier(String qualifier) {
		this.qualifier = qualifier;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
}
